#[Enonic STK](https://enonic.com/stk "Enonic STK Documentation")

##Develop faster with the STK

All craftsmen require tools to do their job. For building sites on Enonic CMS, the STK is that tool.
More specific, the Enonic Standard Templating Kit is a framework for best practice web development
on Enonic CMS. The STK will help you create your websites faster and with higher consistency,
ensuring a smooth project delivery.

##Added gruntjs support

Optional use of gruntjs requires installation of [NodeJs](http://nodejs.org "NodeJs webpage") and [Grunt](http://gruntjs.com/getting-started "Gruntjs getting started guide").
This enables you to run tasks like *grunt less* and and *grunt watch* in console to compile and
compress less -> css out of the box. Further configuring is possible by extending the Gruntfile.js
and package.json files.

##Responsive theme support

Creating responsive web sites with the Enonic STK is easy. Server-side components (RESS) handle
scaling of images which ensure that you serve an optimal image size to any given client.

##Don't reinvent the wheel

As a web developer, you probably have found yourself reinventing the wheel a lot of times.
"Didn't I do this on the previous project?". The STK features an extensive collection of templates
and functions which will reduce the amount of time you spend on such repetitive tasks.

##Training

The official developer training courses held by Enonic is built around the STK. If you are going
to develop sites on Enonic CMS, we recommend that you attend these courses.

##Requirements

We always recommend using the latest version available of Enonic CMS. Most of the features in the
STK are, however, compatible down to 4.5. For full responsive support, Enonic CMS 4.7.4 or later is required.

##Open source

The Enonic STK is of course open source. Feel free to modify it for your own use.

##Getting started

Getting started using the STK is easy! In fact, you can be up and running with your first page
(with mobile support) in a matter of minutes. Just follow the [getting started guide](https://enonic.com/stk/articles/getting-started "Getting started guide"), or browse
the [articles](https://enonic.com/stk/articles "STK Articles").

##Help us

The STK can always be improved. Don't hesitate to contact us if you have developed something that may be a
useful addition to the STK. All feedback will be appreciated. Please report any issues here
(on GitHub).